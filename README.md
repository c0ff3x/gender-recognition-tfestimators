**Warning**: *This project has been migrated to the [following repository](https://gitlab.com/oscar-morales/gender-recognition-tfestimators). Any update to the project will be made there*, see FAQ.

# gender-recognition-tfestimators
Python application for gender classification based on grey scale images using tensorflow estimators

Python (CLI) application for gender classification based on grey scale images of human faces using Tensoflow tfestimators, the images are subjected to different pre-processing techniques before being used for training and testing. The application can make predictions on a group of images or on a sigle one in command line.

---

## FAQ

<details open>
<summary> Migration of the project</summary>
<br>
Due to some health problems I have been obligated to stop the development of the project. The project was passing throughout a complete re-write, so many files are not working properly, if you clone this repo using git and try to run it, the execution will fail, make sure to clone the repository from the link above if you want a program that works properly.
</details>

<details open>
<summary>Will the project receive any updates?</summary>
<br>
The prgram will receive new features like a UI, an update to use Tensorflow 2 and more, also there are instructions of how to run the program in the link above, how to contribute and more.
</details>

<details open>
<summary>Does this repository will receive any updates?</summary>
<br>
No, the project was moved to another account linked above and I encourage you to clone the project from that repository.
</details>
